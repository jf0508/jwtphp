<?php
// 此为Login.php
 
namespace app\index\controller;
use think\Controller;
use app\index\controller\ResponseJson;
// 引入jwt
use think\JWT;
class Login extends controller
{
    use ResponseJson;
 
    /**
     * 编写生成token方法
     * @return [type] [description]
     */
    public function login(){
        // 引用config 下的 jwt.php
        $key =config('key');  //这里是自定义的一个随机字串，应该写在config文件中的，解密时也会用，相当    于加密中常用的 盐  salt
        $jwtData = [
            "iat" => config('lat'), //签发时间
            "nbf" => config('nbf'), //在什么时候jwt开始生效  （这里表示生成10秒后才生效）
            "exp" => config('exp'), //token 过期时间
            "uid" => '12', //记录的userid的信息，这里是自已添加上去的，如果有其它信息，可以再添加数组的键值对
            'password' => '123456',
            'mobile' => '654321' // 电话号码
        ];
 
        $jwtToken = JWT::encode($jwtData, $key);
        return $this->jsonSuccessData([
                'token' =>$jwtToken,
        ]);
    }
 
}

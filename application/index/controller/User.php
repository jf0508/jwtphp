<?php

namespace app\index\controller;
use app\index\model\User as Users;
use app\index\controller\ResponseJson;
use app\index\controller\MyJwtAuth;
use think\Controller;
 
// 引入jwt
use think\JWT;
 
class User extends controller
{
    use ResponseJson;

 
    // 客户端获取用户信息接口
    public function getUser($token=[]){
        if(!$token){
            echo '请传入token';die;
        }

        try {
                $jwtAuth = MyJwtAuth::getInstance(); // 调用单例句柄
                $info = $jwtAuth->checkJwtToken($token);
         
                $in = json_decode($info,true);

                echo "<pre>";
                var_dump($in);exit();

        }catch (\Exception $e) {  //如书写为（Exception $e）将无效
            echo $e->getMessage();
        }

    }
 
}
 
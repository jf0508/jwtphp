<?php

namespace app\index\controller;

// 引入jwt
use think\JWT;
class MyJwtAuth{
 
    private $token;
    private $uid;
    private $key = 'yangguang'; //自定义的key值
 
    /**
     * 单例模式0 JwtAuth句柄
     * @var [type]
     */
    private static $instance;
    private function __construct()
    {
 
    }
 
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }
 
 
    //阻止用户复制对象实例
    private function __clone()
    {
        trigger_error('禁止克隆' ,E_USER_ERROR);
    }
 
    /**
     * 获取token
     * @return [type] [description]
     */
    public function getToken(){
        return (string)$this->token;
    }
 
    /**
     * 设置token
     * @param [type] $token [description]
     */
    public function setToken($token){
        $this->token  =  $token;
        return $this;
    }
 
    /**
     * 获取uid
     * @return [type] [description]
     */
    public function getUid(){
        return (string)$this->uid;
    }
 
    /**
     * 设置uid
     * @param [type] $token [description]
     */
    public function setUid($uid){
        $this->uid  =  $uid;
        return $this;
    }
 
    /**
     * 编写生成token方法
     * @return [type] [description]
     */
    public function getJwtToken(){
    
        $jwtData = [
            "iss" => '',  //签发者 可以为空
            "aud" => '', //面象的用户，可以为空
            "lat" => time(), //签发时间
            "nbf" => time()+10, //在什么时候jwt开始生效  （这里表示生成10秒后才生效）
            "exp" => time()+3600, //token 过期时间 (表示1小时后过期)
            "uid" => $this->uid, //记录的uid的信息，如果有其它信息，可以再添加数组的键值对
            // 'mobile' => $mobile,
        ];
        $this->token = JWT::encode($jwtData, $this->key);
        return $this;
    }
 
     /**
     * 验证token  获取信息方法
     * @param  [type] $token [description]
     * @return [type]        [description]
     */
    public function checkJwtToken($token)
    {
        $info = JWT::decode($token, $this->key,['HS256']);
        return json_encode($info);
    }
 
 
}

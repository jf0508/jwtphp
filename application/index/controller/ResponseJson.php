<?php
// 此为ResponseJson.php
namespace app\index\controller;
trait ResponseJson
{
 
    // 当接口出现异常时的返回
    public function jsonData($code,$message,$data = [])
    {
        return $this->jsonResponse($code,$message,$data);
    }
 
    // app接口请求成功的返回
    public function jsonSuccessData($data = [])
    {
        return $this->jsonResponse(0,'success',$data);
    }
 
    // 返回一个json
	private function jsonResponse($code,$message,$data = [])
	{
		$content = [
			'code'=>$code,
			'message'=>$message,
			'data'=>$data,
		];
		return json_encode($content);
	}
 
}
